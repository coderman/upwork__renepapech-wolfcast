# Wolfcast HUGO Theme

## Site configuration

### Google Tag Manager
Settings can be found in the site configuration file - `config.yml`. <br>
```
params:
  google:
     tag_manager: ...
```

## Content creation

### Post
To generate a new post automatically run the following: <br>
`hugo new --kind posts posts/some-post-name-goes-here`

Post meata data:
* post is automatically set to _draft_; change to _true_ in order for the post to get published
* default post category is type of **wolfcast**; change to **event** as necessary
* to add an author simply add an array item to the **authors key**; for example: `- Max Mustermann`; make sure that the author with the same name already exists
* generic "featured" image is automatically created with every post; to change it simply put a **jpg** type image in the post's respective _img/featured_ folder; make sure that there is only **one** featured image file at any one time :warning:
* to add _Spotify_ playlist insert the URL album code; for example for `https://open.spotify.com/album/4RuzGKLG99XctuBMBkFFOC` add `spotify: 4RuzGKLG99XctuBMBkFFOC`; disabled by default (`false`)

### Author
To generate a new author automatically run the following: <br>
`hugo new --kind authors authors/max-mustermann`

Avatar image is to be placed in the _assets/images/authors_ folder. 
