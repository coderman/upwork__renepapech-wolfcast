---
title: "Footer Small"
date: 2021-01-26T17:57:24+01:00
draft: false
headless: true
social:
  linkedin:
    url: 'http://www.linkedin.com/company/riskwolf'
---

Copyright {{< year >}}, Riskwolf AG. All Rights Reserved.
