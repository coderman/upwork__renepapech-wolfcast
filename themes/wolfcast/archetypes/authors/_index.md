---
title: "{{ replace .Name "-" " " | title }}"
headless: true
avatar:
 dpi: /assets/images/authors/default.jpg
 dpi2: /assets/images/authors/default@2x.jpg
---
